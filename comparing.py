import cv2
import numpy as np
import matplotlib.pyplot as plt

def display_images_with_LSB(image1_path, image2_path):
    # Read the two input images
    img1 = cv2.imread(image1_path)
    img2 = cv2.imread(image2_path)
    
    # Convert images to grayscale
    img1_gray = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    img2_gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

    # Create a figure with three subplots
    fig, axs = plt.subplots(2, 2, figsize=(10, 10))

    # Display the first image in the left subplot
    axs[0, 0].imshow(cv2.cvtColor(img1, cv2.COLOR_BGR2RGB))
    axs[0, 0].set_title('Image 1')
    axs[0, 0].axis('off')
    
    # Display the second image in the middle subplot
    axs[0, 1].imshow(cv2.cvtColor(img2, cv2.COLOR_BGR2RGB))
    axs[0, 1].set_title('Image 2')
    axs[0, 1].axis('off')

    # Display the LSB image in the right subplot
    axs[1, 0].imshow(img1_gray & 1, cmap='gray')
    axs[1, 0].set_title('LSB Image1')
    axs[1, 0].axis('off')
    
    # Display the LSB image in the right subplot
    axs[1, 1].imshow(img2_gray & 1, cmap='gray')
    axs[1, 1].set_title('LSB Image2')
    axs[1, 1].axis('off')

    # Adjust spacing between subplots
    plt.subplots_adjust(wspace=0.01)

    # Show the plot
    plt.show()

# Example usage
image1_path = "car.jpg"
image2_path = "car_encoded.png"

display_images_with_LSB(image1_path, image2_path)