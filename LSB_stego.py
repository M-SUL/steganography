
import cv2

# Function to encode the message into the image using LSB technique
def encode_image(image_path, message, output_path):
    img = cv2.imread(image_path)

    # Check if the message can fit in the image
    if len(message) * 4 > img.shape[0] * img.shape[1] * 3:
        return

    # Convert the message to binary
    binary_message = ''.join(format(ord(char), '08b') for char in message)

    # Counter variables
    index = 0

    # Iterate over each pixel
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            # Modify the last 2 bits of each color channel
            for k in range(3):
                if index < len(binary_message):
                    img[i, j, k] = (img[i, j, k] & 0xFC) | (int(binary_message[index], 2) & 0x03)
                    index += 1

            if index >= len(binary_message):
                break

        if index >= len(binary_message):
            break

    # Save the modified image
    cv2.imwrite(output_path, img)

# Function to decode the message from the image using LSB technique
def decode_image(image_path):
    img = cv2.imread(image_path)

    binary_message = ""
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            for k in range(3):
                binary_message += str(img[i, j, k] & 1)

    decoded_message = ""
    for i in range(0, len(binary_message), 8):
        byte = binary_message[i:i+8]
        char = chr(int(byte, 2))
        if char == '\x00' or not char.isprintable():
            break
        decoded_message += char

    return decoded_message

# usage
image_path = "car.jpg"
output_path = "car_encoded.png"

message_file = open("message.txt", "r")
message_file = message_file.read()


# Encode the message into the image
encode_image(image_path, message_file, output_path)

# Decode the message from the image
decoded_message = decode_image(output_path)
print("Decoded message:", decoded_message)